#include <Geometry/Geometry.h>
#include <System/Settings.h>
using System::getSettings;

namespace Geometry
{
    bool outOfBounds(const glm::vec2 &position,
                     const float &width, const float &height)
    {
        return position.x + width / 2.0  < 0 or
               position.x - width / 2.0  > getSettings().RealWidth - 1 or
               position.y + height / 2.0 < 0 or
               position.y - height / 2.0 > getSettings().RealHeight - 1;
    }
}
