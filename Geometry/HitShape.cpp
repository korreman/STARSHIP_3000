#include <Geometry/HitShape.h>
using std::string;
using std::cout;

namespace Geometry
{
    void HitShape::add(const double &X1, const double &Y1,
                        const double &X2, const double &Y2)
    {
        m_hitboxes.push_back(Rectangle {X1, Y1, X2, Y2});
    }

    bool HitShape::collidesWith(const glm::vec2 &offset,
                                 const HitShape &o,
                                 const glm::vec2 &offset2)
    {
        for (auto i : m_hitboxes)
        {
            for (auto j : o.m_hitboxes)
            {
                if (i.collidesWith(offset, j, offset2))
                {
                    return true;
                }
            }
        }
        return false;
    }

    std::ostream& operator<< (std::ostream &out, const HitShape &h)
    {
        out << "Hitboxes:\n";
        for (auto i : h.m_hitboxes)
        {
            out << i << '\n';
        }
        return out;
    }
}
