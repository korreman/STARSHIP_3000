#include <Geometry/Rectangle.h>
#include <exception>

namespace Geometry
{
    Rectangle::Rectangle(const double &x1, const double &y1,
                         const double &x2, const double &y2)
        : X1(x1), Y1(y1), X2(x2), Y2(y2)
    {
        if (X1 > X2 or Y1 > Y2)
        {
            throw std::invalid_argument(
                    "Attempted to construct negative rectangle. Second "
                    "coordinates must be greater than first coordinates.");
        }
    }

    bool Rectangle::collidesWith(const glm::vec2 &offset,
                                 const Rectangle &other,
                                 const glm::vec2 &offset2)
    {
        return not (X1 + offset.x > other.X2 + offset2.x or
                    Y1 + offset.y > other.Y2 + offset2.y or
                    X2 + offset.x < other.X1 + offset2.x or
                    Y2 + offset.y < other.Y1 + offset2.y);
    }

    std::ostream& operator<< (std::ostream &out, const Rectangle &r)
    {
        out << "("
            << r.X1 << ", "
            << r.Y1 << ", "
            << r.X2 << ", "
            << r.Y2 << ")";
        return out;
    }
}
