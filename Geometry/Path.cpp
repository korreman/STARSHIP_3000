#include <Geometry/Path.h>
#include <cereal/types/list.hpp>
#include <fstream>
#include <iostream>
using std::cout;
using glm::vec2;
using std::string;


namespace Geometry
{
    void Path::addSegment(const double &duration,
                          const vec2 &P1,
                          const vec2 &P2,
                          const vec2 &P3)
    {
        Bezier segment {duration, P1, P2, P3};
        m_segments.push_back(segment);
        m_duration += segment.Duration;
    }

    vec2 Path::getPosition(glm::vec2 position, double time)
    {
        if (m_repeat)
        {
            time = fmod(time, m_duration);
        }

        if (time < 0.0) { return position; }
        for (auto i {m_segments.begin()}; i != m_segments.end(); ++i)
        {
            if (time < i->Duration)
            {
                position = getBezierPosition(position, time, *i);
                return position;
            }
            else
            {
                time -= i->Duration;
                position += i->P3;
            }
        }
        return position;
    }

    vec2 Path::getBezierPosition(vec2 position, const double &time,
                                 const Bezier &segment)
    {
        if (time < 0.0) { return position; }
        else if (time > segment.Duration) { return segment.P3; }
        else
        {
            float t = time / segment.Duration;
            float u = 1.0 - t;

            // Formula taken from Wikipedias article on Bezier curves
            return position + 3.0f * u*u * t * segment.P1 +
                              3.0f * u * t*t * segment.P2 +
                              t*t*t * segment.P3;
        }
    }
}
