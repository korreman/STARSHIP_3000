#version 330 core

in float f_frame;
in vec2 f_texCoord;
in vec3 f_color;
in float f_colorFill;
in float f_alpha;

out vec4 fragOut;

uniform sampler2D f_texture;
uniform int frameCount;

void main()
{
    // Offset texture coordinate according to current frame
    vec2 texCoord = vec2(f_texCoord.x / frameCount, f_texCoord.y);
    texCoord.x = texCoord.x + f_frame / frameCount;

    // Texture color
    vec4 textureColor = texture(f_texture, texCoord);

    // Fill color keeps the alpha channel of texture color
    vec4 fillColor = vec4(f_color, textureColor.a);

    // Final color is a mix between the two, multiplied by an alpha modifier
    vec4 finalColor = mix(textureColor, fillColor, f_colorFill);

    // Set opacity
    fragOut = finalColor * vec4(vec3(1.0), f_alpha);
}
