#version 330 core

layout (location = 0) in vec2 v_position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec2 v_offset;
layout (location = 3) in float frame;
layout (location = 4) in vec3 color;
layout (location = 5) in float colorFill;
layout (location = 6) in float alpha;

out float f_frame;
out vec2 f_texCoord;
out vec3 f_color;
out float f_colorFill;
out float f_alpha;

uniform vec2 winSize;
uniform vec2 texSize;

void main()
{
    // Passthrough
    f_texCoord = texCoord;
    f_color = color;
    f_colorFill = colorFill;
    f_alpha = alpha;
    f_frame = frame;

    // Position is scaled by texture size
    vec2 position = v_position * texSize;

    // Final position is scaled by window size
    gl_Position = vec4((position + 2.0 * floor(v_offset)) / (winSize) - 1.0,
                       0.0, 1.0);
}
