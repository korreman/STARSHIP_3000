#version 330 core
layout (location = 0) in vec2 v_position;
layout (location = 1) in vec4 color;

out vec4 f_color;
uniform vec2 winSize;

void main()
{
    f_color = color;
    gl_Position = vec4(2.0 * v_position / (winSize - 1.0) - 1.0, 0.0, 1.0);
}
