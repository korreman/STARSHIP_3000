#pragma once

#include <Engine/GameObject.h>
#include <Engine/Container.h>
#include <SDL2/SDL.h>

namespace Engine
{
    class Game;

    class Scene : public GameObject
    {
        protected:
            Game &m_game;
            Container m_objects;

        public:
            Scene(Game &game);
            void update(const double &deltaTime);
            std::shared_ptr<DrawList> getDrawables();

            virtual void sendKey(SDL_KeyboardEvent key) = 0;
            virtual void sendButton(SDL_ControllerButtonEvent event) {};
            virtual void sendAxis(SDL_ControllerAxisEvent event) {};
    };
}

