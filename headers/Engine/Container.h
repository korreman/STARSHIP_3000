#pragma once

#include <Engine/GameObject.h>

namespace Engine
{
    class Container : public GameObject
    {
        private:
            std::list<std::shared_ptr<GameObject>> m_objects;

        public:
            void add(std::shared_ptr<GameObject> object);
            void update(const double &deltaTime);
            std::shared_ptr<DrawList> getDrawables();

            auto begin() { return m_objects.begin(); }
            auto end() { return m_objects.end(); }
    };
}

