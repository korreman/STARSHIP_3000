#pragma once

#include <SDL2/SDL_opengl.h>

struct Sprite;

namespace Engine
{
    class Image;
    struct Drawable
    {
        virtual ~Drawable() {}
    };

    struct DrawableImage : Drawable
    {
        Image& image;
        Sprite& sprite;
        DrawableImage(Image &i, Sprite &s) : image(i), sprite(s) {}
    };

    struct DrawableShapes : Drawable
    {
        GLenum type;
        void *data;
        int size;
        DrawableShapes(GLenum t, void *d, const int s)
            : type(t), data(d), size(s) {}
    };
}
