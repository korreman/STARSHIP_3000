#pragma once

#include <list>
#include <memory>

namespace Engine
{
    struct Drawable;
    class GameObject;

    typedef std::list<std::shared_ptr<Drawable>> DrawList;
    typedef std::list<std::shared_ptr<GameObject>> ObjectList;
    class GameObject
    {
    protected:
        std::shared_ptr<ObjectList> m_children;
    public:
        virtual void update(const double &deltaTime) = 0;
        virtual bool isAlive() { return true; }

        virtual std::shared_ptr<DrawList> getDrawables()
        {
            return std::make_shared<DrawList>();
        }

        virtual std::shared_ptr<ObjectList> getChildren() { return m_children; }
    };
}
