#pragma once

#include <SDL2/SDL.h>

namespace Engine
{
    class Session
    {
        private:
//            SDL_Event windowEvent;

        public:
            SDL_Window* window = nullptr;
            SDL_GLContext context;

            Session();
            ~Session();

            void init();
            void close();
    };
}
