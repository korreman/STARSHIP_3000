#pragma once

#include <Engine/GameObject.h>
#include <list>

namespace Engine
{
    class Container;

    /** Calls collision methods between entities in pairs of Containers */
    class CollisionHandler : public GameObject
    {
    private:
        struct ContainerPair { Container &A; Container &B; };
        std::list<ContainerPair> m_pairs;

        void checkCollisions(Container &A, Container &B);
    public:
        void update(const double &deltaTime);
        void addPair(Container &A, Container &B);
    };
}

