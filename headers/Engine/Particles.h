#pragma once

#include <Engine/GameObject.h>
#include <glm/glm.hpp>
#include <random>
#include <list>
#include <vector>

enum BURSTenum
{
    BURST_EXPLOSION,
    BURST_CIRCLE,
    BURST_WAVE,
    BURST_HIT
};

namespace Engine
{
    class Particles : public GameObject
    {
        struct Particle
        {
            glm::vec2 Position {0};
            glm::vec4 Color {1};
            glm::vec2 Velocity {0};
            bool AlphaFade {false};
            double Duration {1};
            double Health {1};
        };

        struct Burst
        {
            // count
            double c {50};
            double cDev {5};
            bool cGauss {true};

            // angle
            double aDev {3.14};
            bool aGauss {false};

            // speed
            double s {50};
            double sDev {50};
            bool sGauss {false};

            // duration
            double d {1};
            double dDev {0.5};
            bool dGauss {true};
        };

    private:
        std::list<Particle> m_particles;
        double m_drag {1.0};

        std::ranlux48_base m_generator;
        std::uniform_real_distribution<> m_uniDist;
        std::normal_distribution<> m_gaussDist;

        const std::vector<Burst> m_bursts
        {
            {
                50, 10, true,
                3.14159, false,
                50, 50, false,
                1.0, 0.3, false
            },
            {
                70, 10, true,
                3.14159, false,
                50, 5, true,
                1.0, 0.3, false
            },
            {
                60, 10, true,
                3.14159/3, true,
                50, 10, true,
                1.2, 0.3, false
            },
            {
                30, 10, true,
                3.14159/8, false,
                60, 50, false,
                0.6, 0.3, false
            }
        };

    public:
        Particles(const double &drag);
        void update(const double &deltaTime);
        std::shared_ptr<DrawList> getDrawables();

        void makeParticle(const glm::vec2 &position,
                          const glm::vec2 &velocity,
                          const glm::vec4 &color,
                          const double &duration,
                          const bool &alphaFade);

        glm::vec2 angleToVec(const double &angle);

        double randomize(const double &mean,
                         const double &deviation = 0,
                         const bool &gaussian = false);

        void makeBurst(const BURSTenum &type,
                       const glm::vec2 &position,
                       const double &angle = 0,
                       const double &size = 1,
                       const double &density = 1,
                       const glm::vec4 &color = glm::vec4 {1});
    };
}
