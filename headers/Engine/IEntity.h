#pragma once

#include <list>
#include <memory>

namespace Engine
{
    class Drawable;

    class IEntity
    {
    public:
        virtual void update(const double &deltaTime) = 0;
        virtual bool isAlive() { return true; }

        virtual std::list<std::shared_ptr<Drawable>>& getDrawables() = 0;
        virtual std::list<std::shared_ptr<Entity>>& getChildren() = 0;
    };
}

