#pragma once

#include <Engine/Entity.h>

namespace Engine
{
    class AnimationEntity : public Entity
    {
    public:
        AnimationEntity(
            Image &image,
            Geometry::HitShape &hitShapes,
            const glm::vec2 &startPos,
            const glm::vec2 &startVel = glm::vec2());
        bool isAlive();
    };
}

