#pragma once

#include <Engine/GameObject.h>
#include <Geometry/Geometry.h>
#include <vector>
#include <glm/glm.hpp>

namespace Geometry {struct Line;}
namespace Engine
{
    class MovingLines : public GameObject
    {
        protected:
            std::vector<Geometry::Line> m_particles;
            std::vector<glm::vec2> m_velocities;

        public:
            void add(const glm::vec2 &posA,
                     const glm::vec2 &posB,
                     const glm::vec2 &velocity,
                     const glm::vec4 &colorA,
                     const glm::vec4 &colorB);

            void add(const glm::vec2 &posA,
                     const glm::vec2 &posB,
                     const glm::vec2 &velocity,
                     const glm::vec4 &color);

            void add(const glm::vec2 &pos,
                     const glm::vec2 &velocity,
                     const glm::vec4 &color);

            void update(const double &deltaTime);
            void remove(const int &index);
            void clear();

            std::shared_ptr<Engine::DrawList> getDrawables();
            std::vector<Geometry::Line>& getLines() { return m_particles; }
    };
}
