#pragma once

#include <list>
#include <memory>

namespace Engine
{
    struct Drawable;
    typedef std::list<std::shared_ptr<Drawable>> DrawList;

    class Renderer
    {
    public:
        void draw(std::shared_ptr<Drawable> drawable);
        void draw(std::shared_ptr<DrawList> drawList);
    };
}

