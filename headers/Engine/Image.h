#pragma once

#include <string>
#include <memory>
#include <cereal/cereal.hpp>

namespace Render { class Texture; }
namespace Engine
{
    class Image
    {
    public:
        std::shared_ptr<Render::Texture> Texture;
        int Width {1}, Height {1};
        int FrameCount {1};
        double FPS {1};

        void loadTexture(std::string filename);
        template<class Archive>
        void load(Archive &archive)
        {
            std::string filename;
            archive(cereal::make_nvp("Filename", filename),
                    cereal::make_nvp("Frame count", FrameCount),
                    cereal::make_nvp("FPS", FPS));
            loadTexture(filename);
        }
    };
}

