#pragma once

#include <Engine/GameObject.h>
#include <Geometry/Sprite.h>
#include <string>

namespace Geometry {class HitShape;}

namespace Engine
{
    class Image;

    class Entity : public GameObject
    {
    protected:
        /** Entity sprite, holds position and rendering info. */
        Sprite m_sprite;
        double m_colorFallOff {1.0};

        /** Reference to sprite position. */
        glm::vec2 &m_position {m_sprite.position};

        /** Sprite is moved by this when update() called. */
        glm::vec2 m_velocity;

        /** Image to draw. */
        Image &m_image;

        /** The starting time of an animation relative to elapsedTime. Useful
         * for controlling the sync of an animation. */
        double m_frameStartTime {0.0};

        /** Increased by deltaTime when update() called */
        double m_elapsedTime {0.0};

        /** A set of hitboxes, used in the checkCollision method */
        Geometry::HitShape &m_hitShapes;

        /** Health field. update() returns false if <= 0. */
        int m_health {1};

        /** Collision action.
         * This method is called if checkCollision discovers a collision with
         * the other object.
         * @param e The entity that collides with this entity.
         */
        virtual void collide(Entity &e) {}

    public:
        Entity(Image &image,
               Geometry::HitShape &hitShapes,
               const glm::vec2 &startPos,
               const glm::vec2 &startVel = glm::vec2());

        void update(const double &deltaTime);
        bool isAlive() { return m_health > 0; }
        std::shared_ptr<DrawList> getDrawables();

        void checkCollision(Entity &e);
        virtual void damage(const int &damage);
        virtual void blit(const glm::vec3 &color,
                          const double &duration = 1.0,
                          const double &release = 0.0);
    };
}
