#pragma once

#include <Engine/Session.h>

namespace Engine
{
    class Scene;
    class Renderer;
    class Game
    {
        private:
            Session m_session;
            Renderer &m_renderer;
            Scene *m_currentScene;
        public:
            Game();
            void run(Scene *startScene);
            void switchScene(Scene *scene);
    };
}
