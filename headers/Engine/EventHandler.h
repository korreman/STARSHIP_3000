#pragma once

namespace Engine
{
    class Scene;

    class EventHandler
    {
        private:
            bool m_shouldQuit {false};

            void addController(const int &id);
            void removeController(const int &id);

        public:
            EventHandler();
            void Poll(Scene &keyReciever);
            bool shouldQuit();
    };
}

