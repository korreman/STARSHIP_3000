#pragma once

#include <cereal/types/list.hpp>
#include <glm/glm.hpp>
#include <list>
#include <string>
#include <Geometry/SerializeGLM.h>

namespace Geometry
{

class Path
{
    struct Bezier
    {
        double Duration {1.0};
        glm::vec2 P1;
        glm::vec2 P2;
        glm::vec2 P3;

        template<class Archive>
        void serialize(Archive &ar)
        {
            ar(CEREAL_NVP(Duration),
               CEREAL_NVP(P1),
               CEREAL_NVP(P2),
               CEREAL_NVP(P3));
        }
    };

private:
    double m_duration {0.0};
    bool m_repeat {false};
    std::list<Bezier> m_segments;
    glm::vec2 getBezierPosition(glm::vec2 position, const double &time,
                                const Bezier &segment);

public:
    void addSegment(const double &duration,
                    const glm::vec2 &P1,
                    const glm::vec2 &P2,
                    const glm::vec2 &P3);
    glm::vec2 getPosition(glm::vec2 position, double time);

    template<class Archive>
    void serialize(Archive &archive)
    {
        archive(cereal::make_nvp("Repeat", m_repeat),
                cereal::make_nvp("Segments", m_segments));
        for (auto i {m_segments.begin()}; i != m_segments.end(); ++i)
        {
            m_duration += i->Duration;
        }
    }
};

}
