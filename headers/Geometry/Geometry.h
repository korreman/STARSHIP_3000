#pragma once

#include <glm/glm.hpp>
#include <tuple>

namespace Geometry
{
    bool outOfBounds(const glm::vec2 &position,
                     const float &width = 0.0, const float &height = 0.0);
    struct Line
    {
        glm::vec2 posA;
        glm::vec4 colA;
        glm::vec2 posB;
        glm::vec4 colB;
    };
}

