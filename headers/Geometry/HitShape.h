#pragma once

#include <Geometry/Rectangle.h>
#include <string>
#include <glm/glm.hpp>
#include <list>
#include <cereal/types/list.hpp>

namespace Geometry
{
    class HitShape
    {
    private:
        std::list<Rectangle> m_hitboxes;

    public:
        void add(const double &X1, const double &Y1,
                 const double &X2, const double &Y2);

        bool collidesWith(const glm::vec2 &offset,
                          const HitShape &o,
                          const glm::vec2 &offset2);

        friend std::ostream& operator<< (std::ostream &out, const HitShape &h);

        template<class Archive>
        void serialize(Archive &archive)
        {
            archive(cereal::make_nvp("HitBoxes", m_hitboxes));
        }
    };
}

