#pragma once

#include <cereal/cereal.hpp>
#include <glm/vec2.hpp>

namespace glm
{
    template<class Archive>
    void serialize(Archive& ar, glm::vec2& v2)
    {
        ar(cereal::make_nvp("x", v2.x), cereal::make_nvp("y", v2.y));
    }
}
