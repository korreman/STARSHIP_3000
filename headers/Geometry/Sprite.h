#pragma once

#include <glm/glm.hpp>

#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

struct Sprite
{
    glm::vec2 position {0.0f, 0.0f};
    GLfloat currentFrame {0.0f};
    glm::vec3 color {1.0f};
    GLfloat colorFill {0.0f};
    GLfloat alpha {1.0f};
};

