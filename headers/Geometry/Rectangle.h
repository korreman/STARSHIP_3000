#pragma once

#include <cereal/cereal.hpp>
#include <glm/glm.hpp>

namespace Geometry
{
    /** Rectangle shape for collision detection */
    class Rectangle
    {
    public:
        double X1 {-1.0},
               Y1 {-1.0},
               X2 { 1.0},
               Y2 { 1.0};

        Rectangle() {}
        Rectangle(const double &x1, const double &y1,
                  const double &x2, const double &y2);

        bool collidesWith(const glm::vec2 &offset,
                          const Rectangle &other,
                          const glm::vec2 &offset2);

        template<class Archive>
        void serialize(Archive &ar)
        {
            ar(CEREAL_NVP(X1), CEREAL_NVP(Y1), CEREAL_NVP(X2), CEREAL_NVP(Y2));
        }

        friend std::ostream& operator<< (std::ostream &out, const Rectangle &r);
    };
}

