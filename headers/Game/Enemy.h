#pragma once

#include <Engine/Entity.h>
#include <string>

namespace Geometry {class Path;}

namespace Game
{
    class Enemy : public Engine::Entity
    {
        private:
            Geometry::Path &m_path;
            glm::vec2 m_startPos;
            int m_points {1};

            void collide(Entity &e) {}

        public:
            Enemy(Engine::Image &image,
                  Geometry::HitShape &hitShapes,
                  Geometry::Path &path,
                  const std::string &shootPatternName,
                  const glm::vec2 &position,
                  const int &health,
                  const int &points);

            void update(const double &deltaTime);

            void damage(const int &damage);
    };
}
