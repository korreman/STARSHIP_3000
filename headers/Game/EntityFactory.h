#pragma once

#include <Game/Bullet.h>

namespace Game
{
    // Constructs a bullet from info in the given
    std::shared_ptr<Bullet> makeBullet(const std::string &name);
}

