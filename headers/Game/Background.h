#pragma once

#include <Engine/GameObject.h>
#include <Engine/MovingLines.h>
#include <random>
using Engine::MovingLines;

namespace Game
{
    class Background : public Engine::GameObject
    {
    private:
        MovingLines m_stars;
        const float m_frontSpeed = 200.0;
        const int m_frontHeight = 4;
        const double m_frontSpawnRate = 1.0;

        const float m_backSpeed = 20.0;
        const int m_backHeight = 3;
        const double m_backSpawnRate = 8.0;

        int m_frontSpawnChance;
        int m_backSpawnChance;

        std::ranlux48_base m_rGen; // seed can be whatever
        std::uniform_int_distribution<int> m_PRNG;

        enum STARenum { STAR_FRONT, STAR_BACK };

    public:
        Background();
        void update(const double &deltaTime);
        std::shared_ptr<Engine::DrawList> getDrawables();

        void updateSpawnChances(const double &deltaTime);
        void spawnStar(const STARenum &starType);
    };
}
