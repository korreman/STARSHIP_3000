#pragma once

#include <Engine/Entity.h>
#include <Game/ShootingPattern.h>

enum DIRECTIONenum
{
    DIRECTION_UP,
    DIRECTION_DOWN,
    DIRECTION_LEFT,
    DIRECTION_RIGHT
};

namespace Game
{
    class Player : public Engine::Entity
    {
        protected:
            double m_accelDir[4] {false};
            float m_accelTime;
            float m_maxSpeed;
            float m_friction;
            float m_speed;
            glm::vec2 m_accel;

            ShootingPattern &m_guns;
            bool m_isShooting {false};
            bool m_shouldShoot {false};
            double m_shootingTimer {0.0};
            double m_shootingInterval {0.2};

            void collide(Engine::Entity &e) {}

        public:
            Player();
            void setAcceleration(const DIRECTIONenum &direction,
                                 const double &mode);

            void setShooting(const bool &mode);
            bool shouldShoot();

            glm::vec2 getPosition() { return m_sprite.position; }
            void update(const double &deltaTime);
    };
}

