#pragma once

#include <Engine/Scene.h>
#include <memory>

namespace Game
{
    class Player;
    class Level : public Engine::Scene
    {
    private:
        std::shared_ptr<Player> m_player;
        std::shared_ptr<Engine::Container> m_playerEntities;

    public:
        Level(Engine::Game &game);
        void update(const double &deltaTime);
        void sendKey(SDL_KeyboardEvent key);
        void sendButton(SDL_ControllerButtonEvent event);
        void sendAxis(SDL_ControllerAxisEvent event);
    };
}
