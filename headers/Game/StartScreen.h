#pragma once

#include <Engine/Scene.h>

namespace Game
{
    class StartScreen : public Engine::Scene
    {
    private:
        Engine::Scene &m_nextScene;
    public:
        StartScreen(Engine::Game &game, Engine::Scene &nextScene);
        void sendKey(SDL_KeyboardEvent key);
        void sendButton(SDL_ControllerButtonEvent event);
    };
}

