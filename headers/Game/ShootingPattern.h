#pragma once

#include <Engine/GameObject.h>
#include <Geometry/SerializeGLM.h>
#include <glm/glm.hpp>
#include <vector>
#include <cereal/types/vector.hpp>

namespace Game
{
    class ShootingPattern : public Engine::GameObject
    {
        struct SpawnInfo
        {
            std::string Name;
            glm::vec2 Position;
            glm::vec2 Velocity;
            template<class Archive> void serialize(Archive &archive)
            {
                archive(CEREAL_NVP(Name),
                        CEREAL_NVP(Position), CEREAL_NVP(Velocity));
            }
        };

    private:
        std::vector<SpawnInfo> m_bullets;
        glm::vec2 m_offset {0};
        double m_timer {0};
        double m_period {0};
        bool m_shooting {false};

    public:
        ShootingPattern();
        void update(const double &deltaTime);
        void setOffset(glm::vec2 &offset);
        void addBullets(const double &elapsedTime);
        void setShooting(const bool &mode);

        template<class Archive>
        void serialize(Archive &archive)
        {
            archive(m_bullets);
        }
    };
}
