#pragma once

#include <Engine/Particles.h>

namespace Game
{
    class ParticleTest : public Engine::GameObject
    {
    private:
        Engine::Particles m_particles {0.1};
        double m_elapsedTime {0};

    public:
        void update(const double &deltaTime);
        std::shared_ptr<Engine::DrawList> getDrawables();
    };
}

