#pragma once

#include <Engine/Entity.h>

namespace Engine {class Container;}
namespace Game
{
    class Bullet : public Engine::Entity
    {
    private:
        int m_damage;
    public:
        Bullet(Engine::Image &image,
               Geometry::HitShape &hitShapes,
               const glm::vec2 &position,
               const glm::vec2 &velocity = glm::vec2(),
               const int &damage = 1);
        void set(const glm::vec2 &position, const glm::vec2 &velocity);

        bool isAlive();
        void collide(Entity &e);
    };
}
