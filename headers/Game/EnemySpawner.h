#pragma once

#include <Engine/GameObject.h>
#include <glm/glm.hpp>
#include <cereal/archives/json.hpp>
#include <string>

namespace Engine {class Container;}

namespace Game
{
    class Enemy;

    struct SpawnAction
    {
        double SpawnTime;
        std::string EnemyName;
        std::string PathName;
        glm::vec2 Position;

        template<class Archive>
        void serialize(Archive &ar)
        {
            ar(CEREAL_NVP(SpawnTime),
               CEREAL_NVP(EnemyName),
               CEREAL_NVP(PathName),
               CEREAL_NVP(Position));
        }
    };

    struct EnemyInfo
    {
        std::string TextureName;
        std::string ShootingPatternName;
        int Health;
        int Points;

        template <class Archive>
        void serialize(Archive &ar)
        {
            ar(CEREAL_NVP(TextureName),
               CEREAL_NVP(ShootingPatternName),
               CEREAL_NVP(Health),
               CEREAL_NVP(Points));
        }
    };

    struct EnemyTimePair { double SpawnTime; std::shared_ptr<Enemy> enemy; };

    class EnemySpawner : public Engine::GameObject
    {
    private:
        double m_elapsedTime {0.0};
        std::list<EnemyTimePair> m_enemies;
        std::shared_ptr<Engine::ObjectList> m_children;

    public:
        EnemySpawner();
        EnemySpawner(const std::string &file);

        std::shared_ptr<Enemy> makeEnemy(const std::string &enemyName,
                const std::string &pathName, const glm::vec2 &position);

        void update(const double &deltaTime);
        std::shared_ptr<Engine::ObjectList> getChildren();

        template<class Archive>
        void load(Archive &archive)
        {
            std::list<SpawnAction> timeline;
            archive(cereal::make_nvp("Timeline", timeline));

            for (auto i {timeline.begin()}; i != timeline.end(); ++i)
            {
                auto e = *i;
                auto enemy = makeEnemy(e.EnemyName, e.PathName, e.Position);
                m_enemies.push_back(EnemyTimePair {e.SpawnTime, enemy});
            }
        }
    };
}

