#pragma once

#include <Render/GLObject.h>
#include <vector>

namespace Render
{
    /** Represents a GPU buffer object.
     *
     * Handles the creation, deletion, binding, and uploading of data to a
     * GPU Buffer Object.
     */
    class Buffer : public GLObject
    {
    protected:
        GLenum m_usage; // usually either GL_STATIC_DRAW or GL_DYNAMIC_DRAW
        GLenum m_bufferType; // GL_ARRAY_BUFFER or GL_ELEMENT_ARRAY_BUFFER

    public:
        /** Initialization method, generates a Buffer Object.
         * @param usage Expected usage pattern of the buffer. Does not directly
         * limit usage of buffer, but helps GPU optimize. Usually
         * GL_DYNAMIC_DRAW or GL_STATIC_DRAW.
         *
         * @param bufferType Type of buffer. Usually GL_ARRAY_BUFFER or
         * GL_ELEMENT_ARRAY_BUFFER.
         */
        void init(const GLenum &usage, const GLenum &bufferType);

        ~Buffer(); //!< Destructor, deletes the buffer from GPU.

        void bind();   //!< Binds the buffer in the GPU.
        void unbind(); //!< Unbinds *any* buffer in the GPU.

        /** Uploads data in vector form to the buffer.
         * @param data Data vector to be uploaded.
         */
        template <typename T>
        void uploadData(const std::vector<T> &data)
        {
            bind();
            glBufferData(m_bufferType,
                         data.size() * sizeof(T),
                         data.data(),
                         m_usage);
            unbind();
        }

        void uploadData(const void *data, const int &size);

        /** Uploads data to the buffer.
         * @param data Data to be uploaded.
         */
        template <typename T>
        void uploadData(const T &data)
        {
            bind();
            glBufferData(m_bufferType, sizeof(T), &data, m_usage);
            unbind();
        }
    };
}

