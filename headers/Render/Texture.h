#pragma once

#include <Render/GLObject.h>

namespace Render
{
    /** Represents a GPU texture object.
     *
     *  Handles the creation, uploading to, and deletion of GPU texture objects.
     *  Animations are handled with horizontal-only spritesheets, but are
     *  handled elsewhere.
     */
    class Texture : public GLObject
    {
    protected:
        /** Sets the scaling and wrapping parameters of the bound texture.
         * @param wrapS Value to set GL_TEXTURE_WRAP_S to.
         * @param wrapT Value to set GL_TEXTURE_WRAP_T to.
         * @param minFilter Value to set GL_TEXTURE_MIN_FILTER to.
         * @param magFilter Value to set GL_TEXTURE_MAG_FILTER to.
         */
        void setParameters(const GLint &wrapS, const GLint &wrapT,
                           const GLint &minFilter, const GLint &magFilter);
    public:
        Texture(); //!< Dummy constructor

        /** Creates a texture with the specified width and height, uploads data
         *  from pointer.
         */
        Texture(unsigned char *image, const int &width, const int &height);

        /** Generates an empty texture with a given size.
         * @param width Width of texture in pixels.
         * @param height Height of texture in pixels.
         */
        void init(const int &width, const int &height);

        /** De-allocates the texture from the GPU RAM. */
        void remove();

        void bind(); //<! Binds the texture for modification and rendering.
        void unbind(); //<! Unbinds the currently bound texture.
    };
}

