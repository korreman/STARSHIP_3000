#pragma once

#include <Render/GLObject.h>

namespace Render
{
    /** Represents a GPU framebuffer object.
     *  Handles generation, binding, and deletino of a Framebuffer Object.
     */
    class FBO : public GLObject
    {
    public:
        void init();          //!< Generates the a framebuffer.
        ~FBO() { destroy(); } //!< Destructor, calls destroy().
        void destroy();       //!< De-allocates framebuffer from GPU RAM.
        void bind();          //!< Binds the framebuffer.
        void unbind();        //!< Unbinds the currently bound framebuffer.

        /** Checks whether the framebuffer is complete.
         * For a framebuffer to be complete, four conditions must be met:
         * 1. At least one buffer must be attached to it.
         * 2. There must be at least one color attachment.
         * 3. All attachments must be complete.
         * 4. Each buffer must have the same number of samples.
         * @return true if complete, false if not
         */
        bool isComplete();
    };
}

