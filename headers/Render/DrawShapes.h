#pragma once

#include <Render/ShapeRenderer.h>
#include <vector>

namespace Geometry {struct Line;}

namespace Render
{
    /** Initialize line drawing on GPU.
     * Initializes array objects, buffers, et cetera for using the drawShapes
     * function.
     */
    void shapeInit();

    /** Deallocate line drawing objects from GPU memory. */
    void shapeClose();

    void drawShapes(GLenum &type, void *data, const int &size);
}

