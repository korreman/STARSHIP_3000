#pragma once

#include <glm/glm.hpp>
#include <SDL2/SDL.h>

struct Sprite;

namespace Render
{
    /** Initialize image drawing on GPU.
     * Initializes array objects, buffers, et cetera for using the drawImage
     * function.
     */
    void drawInit();

    /** Deallocate image drawing objects from GPU memory. */
    void drawClose();

    /** Draw image onto screen.
     * Draws an image onto the current framebuffer according to a texture and a
     * sprite.
     * DOCS NEEDS TO BE UPDATED
     */
    void drawImage(const int &textureID, const int &frameCount,
                   const int &width, const int &height, const Sprite &sprite);
}

