#pragma once

namespace Render
{
    /** Initialize objects in GPU for handling a intermediate framebuffer and
     *  drawing it to the screen.
     */
    void screenInit();

    /** Deallocate framebuffer and screen drawing objects from GPU memory. */
    void screenClose();

    /** Draw the framebuffer to the screen.
     * This method detaches the framebuffer, draws it to the window, re-attaches
     * it. It handles viewports and upscaling as well.
     */
    void drawScreen();
}

