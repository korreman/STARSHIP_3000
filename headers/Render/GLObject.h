#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

namespace Render
{
    /** Base class for representing objects in  OpenGL.
     *
     * Holds an integer of type GLuint, which can be used to act as a
     * ''pointer'' to an object in the OpenGL API.
     */
    class GLObject
    {
    protected:
        GLuint m_identifier;

    public:
        operator GLuint() { return m_identifier; } //!< Returns the identifier.
    };
}

