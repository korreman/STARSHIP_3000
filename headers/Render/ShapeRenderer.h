#pragma once

#include <Render/VertexArrayObject.h>
#include <Render/VertexBufferObject.h>
#include <Render/ShaderProgram.h>

namespace Render
{
    class ShapeRenderer
    {
    private:
        ShaderProgram &m_lineShader;
        VAO &m_lineVAO;
        VBO &m_lineVBO;

    public:
        ShapeRenderer();
        void init();
        void close();

        template<typename T>
        void draw(const GLenum &shape, std::vector<T> &data)
        {
            m_lineShader.bind();

            m_lineVBO.uploadData(data);
            m_lineVAO.bind();

            glDrawArrays(shape, 0, data.size() * 2);

            m_lineVAO.unbind();
        }

        void draw(const GLenum &shape, void *data, const int &size);
    };
}

