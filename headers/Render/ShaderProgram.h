#pragma once

#include <Render/GLObject.h>
#include <string>

namespace Render
{
    /** Represents a full GLSL shader program.
     *  Handles the loading, compilation, destruction and binding of a shader
     *  pair.
     */
    class ShaderProgram : public GLObject
    {
    private:
        GLuint loadShader(const std::string &sourcePath,
                          const GLenum &shaderType);
    public:
        /** Loads, compiles, and combines the shaders from given source files.
         * @param vertexSourcePath Path to the vertex shader source file
         * @param fragSourcePath Path to the fragment shader source file
         */
        void init(const std::string &vertexSourcePath,
                  const std::string &fragSourcePath);
        ~ShaderProgram(); //!< De-allocates the shader from GPU memory
        void bind(); //!< Binds the shader program to be currently used
    };
}

