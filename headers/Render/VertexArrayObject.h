#pragma once

#include <Render/GLObject.h>

namespace Render
{
    /** Represents a GPU Vertex Array Object.
     *  Handles generation, destruction, and binding of a Vertex Array Object.
     */
    class VAO : public GLObject
    {
    public:
        void init();   //!< Generates a Vertex Array Object.
        ~VAO();        //!< De-allocates object from GPU memory.
        void bind();   //!< Binds the object.
        void unbind(); //!< Unbinds the currently bound Vertex Array Object.
    };
}

