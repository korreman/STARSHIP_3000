#pragma once

namespace Render
{
    /** Calls the initialization scripts for all rendering methods. */
    void init();

    /** Calls closing scripts for all rendering methods. */
    void close();
}

