#pragma once

#include <Render/Buffer.h>

namespace Render
{
    /** Vertex Buffer Object, expands on the buffer type.
     *  A specific buffer object for vertex buffers, with a method to set the
     *  attribute pointers for the object.
     */
    class VBO : public Buffer
    {
    public:
        /** See parent method. Calls with bufferType set to GL_ARRAY_BUFFER */
        void init(const GLenum &usage);
        /** Configures vertex attribute pointers in a linear fashion.
         *  Sets the vertex attribute pointers according to the given vector.
         *  Every integer in the vector corresponds to the size of the next
         *  attribute, eg. 1 for a float, 3 for a vec3 type.
         *
         *  @param location The location value of the first attribute. The
         *  location is assumed to progress linearly from here.
         *
         *  @param attribLengths Specifies the lengths of each attribute.
         */
        void setPointers(const int &location,
                         const std::vector<int> &attribLengths);
    };
}

