#pragma once

#include <string>

namespace Engine { class Image; }
namespace Game { class ShootingPattern; }
namespace Geometry { class Path; class HitShape;}

namespace System
{
    void loadAssets();
    Geometry::Path& getPath(const std::string &name);
    Geometry::HitShape& getHitShape(const std::string &name);
    Engine::Image& getImage(const std::string &name);
    Game::ShootingPattern& getShootingPattern(const std::string &name);
}

