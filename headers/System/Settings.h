#pragma once

#include <string>

namespace System
{
    class Settings
    {
        public:
            Settings();
            std::string Name {"STARSHIP 3000"};
            int WindowWidth {540};
            int WindowHeight {720};
            int RealWidth {180};
            int RealHeight {240};
            bool Fullscreen {false};

            float MinFPS {30.0};
            float MaxFPS {120.0};
            float TimeScale {1.0};

            void save();
            void load();
    };

    Settings& getSettings();
}
