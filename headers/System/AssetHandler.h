#pragma once

#include <cereal/archives/json.hpp>
#include <fstream>
#include <unordered_map>
#include <list>
#include <string>
#include <exception>

namespace System
{
    template<class T>
    class AssetHandler
    {
    private:
        std::unordered_map<std::string, T> m_assets;
        const std::string m_prefix;
        const std::string m_extension;

        std::list<std::string> loadLineList(const std::string filename)
        {
            std::ifstream in(filename);
            if (in.good())
            {
                std::list<std::string> lines;
                std::string line;
                while (std::getline(in, line))
                {
                    lines.push_front(line);
                }
                return lines;
            }
            throw std::invalid_argument("Asset list not found: " + filename);
        }

        void loadAsset(const std::string &name)
        {
            std::ifstream is(m_prefix + name + m_extension);
            if(is.good())
            {
                T asset;
                cereal::JSONInputArchive archive(is);
                archive(asset);

                m_assets.emplace(std::make_pair(name, asset));
            }
        }

    public:
        AssetHandler(const std::string &prefix, const std::string &extension)
            : m_prefix(prefix), m_extension(extension) { }

        void load()
        {
            auto names = loadLineList(m_prefix + "filelist.txt");
            for (auto name {names.begin()}; name != names.end(); ++name)
            {
                loadAsset(*name);
            }
        }

        T& get(const std::string &name)
        {
            if (not m_assets.count(name))
            {
                throw std::invalid_argument("Asset not found: " +
                                            m_prefix + name + m_extension);
            }
            return m_assets.find(name)->second;
        }
    };

}
