#pragma once

#include <string>
#include <list>

namespace System
{
    // Loads a text file into a string
    std::string loadTextFile(const char *filename);
}

