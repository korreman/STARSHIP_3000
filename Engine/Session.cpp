#include <Engine/Session.h>
#include <System/Settings.h>
#include <iostream>

using System::getSettings;

#define GLEW_STATIC
#include <GL/glew.h>

namespace Engine
{
    Session::Session() { init(); }
    Session::~Session() { close(); }

// Creates an OpenGL context and a window
void Session::init()
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) < 0)
    {
        std::cout << "SDL_Init failed: " << SDL_GetError() << '\n';
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    std::cout << "Creating window...\n";
    Uint32 flags {SDL_WINDOW_OPENGL};
    if (getSettings().Fullscreen) { flags |= SDL_WINDOW_FULLSCREEN_DESKTOP; }

    window = SDL_CreateWindow(getSettings().Name.c_str(),
                              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              getSettings().WindowWidth,
                              getSettings().WindowHeight,
                              flags);
    if (window == NULL)
    {
        std::cout << "Window creation failed: " << SDL_GetError() << '\n';
    }

    std::cout << "Creating context...\n";
    context = SDL_GL_CreateContext(window);
    if (context == NULL)
    {
        std::cout << "Context creation failed: " << SDL_GetError() << '\n';
    }

    std::cout << "Initializing GLEW...\n";
    glewExperimental = GL_TRUE;
    if (glewInit() != 0)
    {
        std::cout << "GLEW initialization failed: " << SDL_GetError() << '\n';
    }
    std::cout << "OpenGL info: \n"
              << "Vendor: " << glGetString(GL_VENDOR) << '\n'
              << "Renderer: " << glGetString(GL_RENDERER) << '\n'
              << "Version: " << glGetString(GL_VERSION) << '\n'
              << "Language version: "
              << glGetString(GL_SHADING_LANGUAGE_VERSION) << '\n';
}

void Session::close()
{
    std::cout << "Deleting context...\n";
    SDL_GL_DeleteContext(context);
    std::cout << "Destroying window...\n";
    SDL_DestroyWindow(window);
    std::cout << "Quitting SDL...\n";
    SDL_Quit();
}

}
