#include <Engine/Entity.h>
#include <Engine/Drawable.h>
#include <Engine/Image.h>
#include <Geometry/HitShape.h>
#include <algorithm>
using std::vector;
using glm::vec2;

namespace Engine
{
    Entity::Entity(Image &image, Geometry::HitShape &hitShapes,
                   const glm::vec2 &startPos, const glm::vec2 &startVel)
        : m_sprite(Sprite{startPos}),
          m_velocity(startVel),
          m_image(image),
          m_hitShapes(hitShapes) {}

    void Entity::update(const double &deltaTime)
    {
        m_elapsedTime += deltaTime;
        m_position += m_velocity * (GLfloat)deltaTime;
        m_sprite.currentFrame = (int)((m_elapsedTime - m_frameStartTime)
                * m_image.FPS) % m_image.FrameCount;

        m_sprite.colorFill -= m_colorFallOff * deltaTime;
        m_sprite.colorFill = std::max(m_sprite.colorFill, 0.0f);
    }

    std::shared_ptr<DrawList> Entity::getDrawables()
    {
        auto retval {std::make_shared<DrawList>()};
        retval->push_front(std::make_shared<DrawableImage>(m_image, m_sprite));
        return retval;
    }

    void Entity::checkCollision(Entity &e)
    {
        if (m_hitShapes.collidesWith(m_position, e.m_hitShapes, e.m_position))
        {
            collide(e);
            e.collide(*this);
        }
    }

    void Entity::damage(const int &damage)
    {
        m_health -= damage;
    }

    void Entity::blit(const glm::vec3 &color,
                      const double &duration,
                      const double &release)
    {
        m_sprite.color = color;
        if (release == 0.0)
        {
            m_colorFallOff = 10000;
        }
        else
        {
            m_colorFallOff = 1.0 / release;
        }
        m_sprite.colorFill = 1.0 + duration * m_colorFallOff;
    }
}
