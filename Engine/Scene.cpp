#include <Engine/Game.h>
#include <Engine/Scene.h>
#include <Render/DrawScreen.h>
#include <SDL2/SDL.h>

namespace Engine
{
    Scene::Scene(Game &game) : m_game(game) {}

    void Scene::update(const double &deltaTime)
    {
        m_objects.update(deltaTime);
    }

    std::shared_ptr<DrawList> Scene::getDrawables()
    {
        return m_objects.getDrawables();
    }
}
