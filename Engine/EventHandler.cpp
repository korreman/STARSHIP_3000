#include <Engine/EventHandler.h>
#include <Engine/Scene.h>
#include <SDL2/SDL.h>
#include <iostream>
using std::cout;

namespace Engine
{
    EventHandler::EventHandler()
    {
        SDL_GameControllerAddMappingsFromFile("assets/gamecontrollerdb.txt");
        cout << "Joysticks: " << SDL_NumJoysticks() << '\n';
        for (int id {0}; id < SDL_NumJoysticks(); ++id)
        {
            addController(id);
        }
    }

    void EventHandler::addController(const int &id)
    {
        if (SDL_IsGameController(id))
        {
            cout << "Controller detected, id: " << id << '\n';
            if (SDL_GameControllerOpen(id))
            {
                cout << "Controller succesfully added.\n";
            }
        }
    }

    void EventHandler::removeController(const int &instanceID)
    {
        SDL_GameController *pad {SDL_GameControllerFromInstanceID(instanceID)};
        SDL_GameControllerClose(pad);
        cout << "Controller disconnected.\n";
    }

    void EventHandler::Poll(Scene &keyReciever)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_CONTROLLERBUTTONDOWN:
                case SDL_CONTROLLERBUTTONUP:
                    keyReciever.sendButton(event.cbutton);
                    break;
                case SDL_CONTROLLERAXISMOTION:
                    keyReciever.sendAxis(event.caxis);
                    break;

                case SDL_KEYDOWN:
                case SDL_KEYUP:
                    keyReciever.sendKey(event.key);
                    break;

                case SDL_CONTROLLERDEVICEADDED:
                    addController(event.cdevice.which);
                    break;
                case SDL_CONTROLLERDEVICEREMOVED:
                    removeController(event.cdevice.which);
                    break;
                case SDL_QUIT:
                    m_shouldQuit = true;
            }
        }
    }

    bool EventHandler::shouldQuit()
    {
        return m_shouldQuit;
    }
}
