#include <Engine/Game.h>

#include <Engine/Scene.h>
#include <Engine/EventHandler.h>
#include <Engine/Renderer.h>
#include <Render/Init.h>
#include <Render/DrawScreen.h>
#include <System/Assets.h>
#include <System/Settings.h>

#include <SDL2/SDL.h>
#include <iostream>
#include <exception>

using System::getSettings;

namespace Engine
{
    Game::Game() : m_renderer(*new Renderer)
    {
        std::cout << "\nInitializing rendering environment...\n";
        Render::init();

        std::cout << "\nLoading assets...\n";
        System::loadAssets();
    }

    void Game::run(Scene *startScene)
    {
        if (startScene == nullptr)
        {
            throw std::invalid_argument("Starting scene cannot be nullptr.");
        }

        m_currentScene = startScene;
        EventHandler eventHandler;
        double maxDeltaTime = 1000.0 / getSettings().MinFPS;
        double currentTime {(double)SDL_GetTicks()};
        double timeScale  = getSettings().TimeScale;

        while (!eventHandler.shouldQuit())
        {
            eventHandler.Poll(*m_currentScene);

            double newTime = SDL_GetTicks();
            double frameTime = newTime - currentTime;

            while (frameTime > 0.0)
            {
                double deltaTime = std::min(frameTime, maxDeltaTime);
                m_currentScene->update(timeScale * deltaTime / 1000.0);
                frameTime -= deltaTime;
            }

            m_renderer.draw(m_currentScene->getDrawables());
            Render::drawScreen();
            SDL_GL_SwapWindow(m_session.window);
            currentTime = newTime;
        }
        std::cout << "Game ended\n";
        std::cout << "\nCleaning up...\n";
        Render::close();
    }

    void Game::switchScene(Scene *scene)
    {
        m_currentScene = scene;
    }

}
