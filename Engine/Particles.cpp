#include <Engine/Particles.h>
#include <Engine/Drawable.h>
#include <algorithm>
#include <vector>
#include <tuple>
#include <math.h>
using std::vector;

namespace Engine
{
    Particles::Particles(const double &drag)
        : m_drag(drag), m_generator(13397),
          m_uniDist(-1.0, 1.0), m_gaussDist(0, 1.0/3.0) {}

    void Particles::update(const double &deltaTime)
    {
        float deltaTimeF {(float)deltaTime};
        for (auto &p : m_particles)
        {
            p.Position += p.Velocity * deltaTimeF;
            p.Health -= 1.0 / p.Duration * deltaTimeF;
            p.Velocity *= (float)pow(m_drag, deltaTime);
            p.Color.a -= p.AlphaFade ? 1.0 / p.Duration * deltaTimeF : 0.0;
        }

        m_particles.erase(
                std::remove_if(m_particles.begin(), m_particles.end(),
                               [](Particle p) { return p.Health <= 0.0; }),
                m_particles.end());
    }

    struct RenderPair {glm::vec2 Position; glm::vec4 Color;};
    std::shared_ptr<DrawList> Particles::getDrawables()
    {
        // convert to drawable shape data
        vector<RenderPair> renderData;
        renderData.reserve(m_particles.size());
        for (auto p : m_particles)
        {
            renderData.push_back(RenderPair {p.Position, p.Color});
        }

        // make drawable list
        auto retval {std::make_shared<DrawList>()};
        retval->push_back(std::make_shared<DrawableShapes>(GL_POINTS,
                    (void*)renderData.data(),
                    renderData.size() * sizeof(RenderPair)));

        return retval;
    }

    void Particles::makeParticle(const glm::vec2 &position,
                                 const glm::vec2 &velocity,
                                 const glm::vec4 &color,
                                 const double &duration,
                                 const bool &alphaFade)
    {
        m_particles.push_back(
                Particle {position, color, velocity, alphaFade, duration});
    }

    glm::vec2 Particles::angleToVec(const double &angle)
    {
        return glm::vec2 {glm::cos(angle), glm::sin(angle)};
    }

    double Particles::randomize(const double &mean, const double &deviation,
            const bool &gaussian)
    {
        return mean + (gaussian ? m_gaussDist(m_generator) * deviation
                                : m_uniDist(m_generator) * deviation);
    }

    void Particles::makeBurst(const BURSTenum &type, const glm::vec2 &position,
            const double &angle, const double &size, const double &density,
            const glm::vec4 &color)
    {
        Burst b {m_bursts[type]};

        int count {(int)(density * size * randomize(b.c, b.cDev, b.cGauss))};
        for (int i {0}; i < count; ++i)
        {
            double newAngle {randomize(angle, b.aDev, b.aGauss)};
            float speed {(float)randomize(size * b.s, size * b.sDev, b.sGauss)};
            double duration {randomize(b.d, b.dDev, b.dGauss)};

            glm::vec4 newColor {color};
            newColor.r = randomize(color.r, 0.2);
            newColor.g = randomize(color.g, 0.2);
            newColor.b = randomize(color.b, 0.2);

            makeParticle(position, angleToVec(newAngle) * speed, newColor,
                    duration, true);
        }
    }
}

