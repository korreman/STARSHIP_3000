#include <Engine/Image.h>
#include <Render/DrawImages.h>
#include <Render/Texture.h>
#include <Render/SOIL.h>

namespace Engine
{
    void Image::loadTexture(std::string filename)
    {
        unsigned char *image = SOIL_load_image(filename.c_str(),
                                               &Width, &Height,
                                               0, SOIL_LOAD_RGBA);
        Texture = std::make_shared<Render::Texture>(image, Width, Height);
        Width /= FrameCount;
        SOIL_free_image_data(image);
    }
}
