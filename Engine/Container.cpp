#include <Engine/Container.h>
#include <algorithm>

namespace Engine
{
    void Container::update(const double &deltaTime)
    {
        ObjectList spawn;
        for (auto &o : m_objects)
        {
            o->update(deltaTime);
            auto n {o->getChildren()};
            if (n)
            {
                spawn.splice(spawn.begin(), *n, n->begin(), n->end());
            }
        }
        m_objects.splice(m_objects.begin(), spawn, spawn.begin(), spawn.end());

        auto begin {m_objects.begin()}, end{m_objects.end()};
        auto isAlive { [&](std::shared_ptr<GameObject> e)
            {return !e->isAlive();} };
        m_objects.erase(std::remove_if(begin, end, isAlive), end);
    }

    std::shared_ptr<DrawList> Container::getDrawables()
    {
        auto retval {std::make_shared<DrawList>()};
        for (auto &o : m_objects)
        {
            auto d {o->getDrawables()};
            retval->splice(retval->end(), *d, d->begin(), d->end());
        }
        return retval;
    }

    void Container::add(std::shared_ptr<GameObject> object)
    {
        m_objects.push_back(object);
    }
}
