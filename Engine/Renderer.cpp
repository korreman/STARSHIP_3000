#include <Engine/Renderer.h>
#include <Engine/Image.h>
#include <Render/DrawShapes.h>
#include <Render/DrawImages.h>
#include <Render/Texture.h>

#define GLEW_STATIC
#include <GL/glew.h> // must be included before OpenGL
#include <Engine/Drawable.h> // this includes OpenGL

namespace Engine
{
    void Renderer::draw(std::shared_ptr<Drawable> drawable)
    {
        auto d1 {std::dynamic_pointer_cast<DrawableImage>(drawable)};
        if (d1)
        {
            Render::drawImage(*(d1->image.Texture), d1->image.FrameCount,
                    d1->image.Width, d1->image.Height, d1->sprite);
            return;
        }

        auto d2 {std::dynamic_pointer_cast<DrawableShapes>(drawable)};
        if (d2)
        {
            Render::drawShapes(d2->type, d2->data, d2->size);
            return;
        }
    }

    void Renderer::draw(std::shared_ptr<DrawList> drawList)
    {
        for (auto &d : *drawList)
        {
            draw(d);
        }
    }
}
