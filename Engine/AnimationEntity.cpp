#include <Engine/AnimationEntity.h>
#include <Engine/Image.h>

namespace Engine
{
    AnimationEntity::AnimationEntity(
            Image &image,
            Geometry::HitShape &hitShapes,
            const glm::vec2 &startPos,
            const glm::vec2 &startVel)
        : Entity(image, hitShapes, startPos, startVel) {}

    bool AnimationEntity::isAlive()
    {
        return not (m_elapsedTime > (double)m_image.FrameCount / m_image.FPS);
    }
}
