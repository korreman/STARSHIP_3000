#include <Engine/CollisionHandler.h>
#include <Engine/Container.h>
#include <Engine/Container.h>
#include <Engine/Entity.h>

namespace Engine
{
    void CollisionHandler::update(const double &deltaTime)
    {
        for (auto &i : m_pairs)
        {
            checkCollisions(i.A, i.B);
        }
    }

    void CollisionHandler::checkCollisions(Container &A, Container &B)
    {
        for (auto &a : A)
        {
            auto A {std::dynamic_pointer_cast<Entity>(a)};
            if (A)
            {
                for (auto &b : B)
                {
                    auto B {std::dynamic_pointer_cast<Entity>(b)};
                    if (B) { A->checkCollision(*B); }
                }
            }
        }
    }

    void CollisionHandler::addPair(Container &A, Container &B)
    {
        m_pairs.push_front(ContainerPair {A, B});
    }
}
