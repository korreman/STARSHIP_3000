#include <Engine/MovingLines.h>
#include <Engine/Drawable.h>
#include <SDL2/SDL_opengl.h>
using Geometry::outOfBounds;
using Geometry::Line;

namespace Engine
{

    void MovingLines::add(const glm::vec2 &posA,
                          const glm::vec2 &posB,
                          const glm::vec2 &velocity,
                          const glm::vec4 &colorA,
                          const glm::vec4 &colorB)
    {
        m_particles.push_back(Line {posA, colorA, posB, colorB});
        m_velocities.push_back(velocity);
    }

    void MovingLines::add(const glm::vec2 &posA,
                          const glm::vec2 &posB,
                          const glm::vec2 &velocity,
                          const glm::vec4 &color)
    {
        add(posA, posB, velocity, color, color);
    }

    void MovingLines::add(const glm::vec2 &pos,
                          const glm::vec2 &velocity,
                          const glm::vec4 &color)
    {
        add(pos, pos, velocity, color, color);
    }

    void MovingLines::update(const double &deltaTime)
    {
        for (unsigned int i {0}; i < m_particles.size(); ++i)
        {
            m_particles[i].posA += m_velocities[i] * (GLfloat)deltaTime;
            m_particles[i].posB += m_velocities[i] * (GLfloat)deltaTime;

            if (outOfBounds(m_particles[i].posA, 0.0, 0.0) and
                outOfBounds(m_particles[i].posB, 0.0, 0.0))
            {
                remove(i);
                --i;
            }
        }
    }

    std::shared_ptr<Engine::DrawList> MovingLines::getDrawables()
    {
        auto retval {std::make_shared<DrawList>()};
        retval->push_front(std::make_shared<DrawableShapes>(
                 GL_LINES, (void*)m_particles.data(),
                 (int)(m_particles.size() * sizeof(Line))));
        return retval;
    }

    void MovingLines::remove(const int &index)
    {
        m_particles.erase(m_particles.begin() + index);
        m_velocities.erase(m_velocities.begin() + index);
    }

    void MovingLines::clear()
    {
        m_particles.clear();
        m_velocities.clear();
    }

}
