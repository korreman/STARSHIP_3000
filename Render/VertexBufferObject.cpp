#include <Render/VertexBufferObject.h>
using std::vector;

namespace Render
{
    void VBO::init(const GLenum &usage)
    {
        Buffer::init(usage, GL_ARRAY_BUFFER);
    }

    void VBO::setPointers(const int &location, const vector<int> &attribLengths)
    {
        // Calculate stride
        int stride {0};
        for (int i {0}, limit {(int)attribLengths.size()}; i < limit; ++i)
        {
            stride += attribLengths[i];
        }

        // Set attribute pointers
        bind();

        // i iterates normally, j sets the offset pointer
        for (int i {0}, j {0}; j < stride; j += attribLengths[i++])
        {
            glEnableVertexAttribArray(i + location);
            glVertexAttribPointer(i + location,
                                  attribLengths[i],
                                  GL_FLOAT, GL_FALSE,
                                  stride * sizeof(GLfloat),
                                  (GLvoid*)(j * sizeof(GLfloat)));
        }

        unbind();
    }
}
