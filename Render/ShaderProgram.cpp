// Handles the loading and compiling of a vertex and fragment shader
#include <Render/ShaderProgram.h>
#include <System/SystemUtils.h>
#include <iostream>
using std::string;
using std::cout;

namespace Render
{
    // Creates a shader program from the sources given. Stores the handle in
    // m_identifier
    void ShaderProgram::init(const string &vertexSourcePath,
                             const string &fragSourcePath)
    {
        // Create shaders
        GLuint vertexShader   {loadShader(vertexSourcePath, GL_VERTEX_SHADER)};
        GLuint fragmentShader {loadShader(fragSourcePath, GL_FRAGMENT_SHADER)};

        // Combine shaders into shader program
        m_identifier = glCreateProgram();
        glAttachShader(m_identifier, vertexShader);
        glAttachShader(m_identifier, fragmentShader);
        glLinkProgram(m_identifier);

        // Check for linking error
        GLint success;
        GLchar infoLog[512];
        glGetProgramiv(m_identifier, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(m_identifier, 512, NULL, infoLog);
            cout << "ERROR - Shader linking failed!\n"
                 << "Vertex shader: " << vertexSourcePath << '\n'
                 << "Fragment shader: " << fragSourcePath << '\n'
                 << infoLog << '\n';
        }

        // Only the shader program is needed
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }

    // Deallocate the shader
    ShaderProgram::~ShaderProgram()
    {
        glDeleteProgram(m_identifier);
    }

    // Loads and compiles the shader at sourcePath, as type shaderType
    // Returns the GLuint handle
    GLuint ShaderProgram::loadShader(const string &sourcePath,
                                     const GLenum &shaderType)
    {
        // Read source
        string shaderSource {System::loadTextFile(sourcePath.c_str())};
        const char *shaderCode {shaderSource.c_str()};

        // Create and compile shader
        GLuint shader {glCreateShader(shaderType)};
        glShaderSource(shader, 1, &shaderCode, NULL);
        glCompileShader(shader);

        // Check for compilation errors
        GLint success;
        GLchar infoLog[512];
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

        if (!success)
        {
            glGetShaderInfoLog(shader, 512, NULL, infoLog);
            cout << "ERROR - Shader compilation failed!\n"
                 << "Shader source: " << sourcePath << '\n'
                 << infoLog << '\n';
        }

        return shader;
    }

    // Bind shader program to be currently used
    void ShaderProgram::bind()
    {
        glUseProgram(m_identifier);
    }
}
