#include <Render/Framebuffer.h>
#include <iostream>

namespace Render
{
    void FBO::init()
    {
        glGenFramebuffers(1, &m_identifier);
    }

    void FBO::destroy()
    {
        glDeleteFramebuffers(1, &m_identifier);
    }

    void FBO::bind()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, m_identifier);
    }

    void FBO::unbind()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    bool FBO::isComplete()
    {
        bind();
        return glCheckFramebufferStatus(GL_FRAMEBUFFER) ==
               GL_FRAMEBUFFER_COMPLETE;
        unbind();
    }
}
