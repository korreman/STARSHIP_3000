#include <Render/Buffer.h>
#include <iostream>
using std::vector;

namespace Render
{
    void Buffer::init(const GLenum &usage, const GLenum &bufferType)
    {
        glGenBuffers(1, &m_identifier);
        m_usage = usage;
        m_bufferType = bufferType;
    }

    Buffer::~Buffer()
    {
        glDeleteBuffers(1, &m_identifier);
    }

    void Buffer::uploadData(const void *data, const int &size)
    {
        bind();
        glBufferData(m_bufferType, size, data, m_usage);
        unbind();
    }

    void Buffer::bind()
    {
        glBindBuffer(m_bufferType, m_identifier);
    }

    void Buffer::unbind()
    {
        glBindBuffer(m_bufferType, 0);
    }
}
