#include <Render/DrawImages.h>
#include <Render/VertexBufferObject.h>
#include <Render/VertexArrayObject.h>
#include <Render/Texture.h>
#include <Render/ShaderProgram.h>
#include <Geometry/Sprite.h>
#include <System/Settings.h>
#include <vector>
using System::getSettings;
using std::vector;
using std::string;

namespace Render
{
    const string insVSource {"shaders/drawImages.vs"};
    const string insFSource {"shaders/drawImages.frag"};

    ShaderProgram &insShader = *new ShaderProgram;
    VAO &insVAO = *new VAO;
    VBO &insVBO = *new VBO;
    VBO &quadVBO = *new VBO;
    Buffer &quadEBO = *new Buffer;

    GLuint texSizeUni;
    GLuint frameCountUni;

    void drawInit()
    {
        vector<GLint> quadElements { 0, 1, 2, 2, 3, 0 };
        vector<GLfloat> quadVertices
        { // Position    // Texture coordinate
            -1.0f,  1.0f,   0.0f, 0.0f,
             1.0f,  1.0f,   1.0f, 0.0f,
             1.0f, -1.0f,   1.0f, 1.0f,
            -1.0f, -1.0f,   0.0f, 1.0f
        };

        vector<int> quadAttribs { 2, 2 };
        vector<int> insAttribs { 2, 1, 3, 1, 1 };

        insVAO.init();
        insVAO.bind();
        insShader.init(insVSource, insFSource);

        quadVBO.init(GL_STATIC_DRAW);
        quadVBO.uploadData(quadVertices);

        quadEBO.init(GL_STATIC_DRAW, GL_ELEMENT_ARRAY_BUFFER);
        quadEBO.uploadData(quadElements);

        quadVBO.setPointers(0, quadAttribs);
        quadEBO.bind();

        insVBO.init(GL_DYNAMIC_DRAW);
        insVBO.setPointers(2, insAttribs);
        for (int i {2}; i <= 6; ++i)
        {
            glVertexAttribDivisor(i, 1);
        }
        insVAO.unbind();

        insShader.bind();
        glUniform2f(glGetUniformLocation(insShader, "winSize"),
                    (GLfloat)getSettings().RealWidth,
                    (GLfloat)getSettings().RealHeight);

        texSizeUni = glGetUniformLocation(insShader, "texSize");
        frameCountUni = glGetUniformLocation(insShader, "frameCount");
    }

    void drawClose()
    {
        delete &insShader;
        delete &quadVBO;
        delete &quadEBO;
        delete &insVBO;
        delete &insVAO;
    }

    void drawImage(const int &textureID, const int &frameCount,
                   const int &width, const int &height, const Sprite &sprite)
    {
        insShader.bind();
        glUniform2f(texSizeUni, width, height);
        glUniform1i(frameCountUni, frameCount);

        insVBO.uploadData(sprite);
        insVAO.bind();
        glBindTexture(GL_TEXTURE_2D, textureID);

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        glBindTexture(GL_TEXTURE_2D, 0);
        insVAO.unbind();
    }
}
