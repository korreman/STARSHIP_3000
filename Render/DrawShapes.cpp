#include <Render/DrawShapes.h>

namespace Render
{
    ShapeRenderer &shapeRenderer = *new ShapeRenderer;

    void drawShapes(GLenum &type, void *data, const int &size)
    {
        shapeRenderer.draw(type, data, size);
    }

    void shapeInit()
    {
        shapeRenderer.init();
    }

    void shapeClose()
    {
        shapeRenderer.close();
    }
}
