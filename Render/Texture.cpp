#include <Render/Texture.h>
#include <Render/SOIL.h>
#include <iostream>

namespace Render
{
    Texture::Texture() {}

    void Texture::setParameters(const GLint &wrapS, const GLint &wrapT,
                                const GLint &minFilter, const GLint &magFilter)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
    }

    void Texture::init(const int &width, const int &height)
    {
        glGenTextures(1, &m_identifier);

        bind();
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                     width, height,
                     0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

        setParameters(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,
                      GL_NEAREST, GL_NEAREST);
        unbind();
    }

    Texture::Texture(unsigned char *image, const int &width, const int &height)
    {
        glGenTextures(1, &m_identifier);
        bind();
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                     width, height,
                     0, GL_RGBA, GL_UNSIGNED_BYTE, image);

        setParameters(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,
                      GL_NEAREST, GL_NEAREST);
        unbind();
    }

    void Texture::remove()
    {
        glDeleteTextures(1, &m_identifier);
    }

    void Texture::bind()
    {
        glBindTexture(GL_TEXTURE_2D, m_identifier);
    }

    void Texture::unbind()
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
