#include <Render/ShapeRenderer.h>
#include <string>
#include <vector>
#include <System/Settings.h>
using System::getSettings;
using std::string;
using std::vector;

namespace Render
{
    ShapeRenderer::ShapeRenderer()
        : m_lineShader(*new ShaderProgram),
          m_lineVAO(*new VAO),
          m_lineVBO (*new VBO) {}

    void ShapeRenderer::init()
    {
        const string lineVSource {"shaders/drawShapes.vs"};
        const string lineFSource {"shaders/drawShapes.frag"};

        vector<int> lineAttribs { 2 , 4 };

        m_lineShader.init(lineVSource, lineFSource);
        m_lineVAO.init();
        m_lineVAO.bind();

        m_lineVBO.init(GL_DYNAMIC_DRAW);
        m_lineVBO.setPointers(0, lineAttribs);

        m_lineVAO.unbind();

        m_lineShader.bind();
        glUniform2f(glGetUniformLocation(m_lineShader, "winSize"),
                    (GLfloat)getSettings().RealWidth,
                    (GLfloat)getSettings().RealHeight);
    }

    void ShapeRenderer::draw(const GLenum &shape, void *data, const int &size)
    {
            m_lineShader.bind();

            m_lineVBO.uploadData(data, size);
            m_lineVAO.bind();

            glDrawArrays(shape, 0, size);

            m_lineVAO.unbind();
    }

    void ShapeRenderer::close()
    {
        delete &m_lineShader;
        delete &m_lineVAO;
        delete &m_lineVBO;
    }
}
