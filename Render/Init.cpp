#include <Render/Init.h>
#include <Render/DrawImages.h>
#include <Render/DrawShapes.h>
#include <Render/DrawScreen.h>
#include <Render/VertexBufferObject.h>
#include <SDL2/SDL.h>
#include <string>
using std::vector;
using std::string;

namespace Render
{
    void init()
    {
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        drawInit();
        shapeInit();
        screenInit();
    }

    void close()
    {
        drawClose();
        shapeClose();
        screenClose();
    }
}
