// Handles creation and binding of Vertex Array Objects (VAOs)
#include <Render/VertexArrayObject.h>
#include <iostream>

namespace Render
{
    // Generates a single VAO
    void VAO::init()
    {
        glGenVertexArrays(1, &m_identifier);
    }

    // De-allocate VAO
    VAO::~VAO()
    {
        glDeleteVertexArrays(1, &m_identifier);
    }

    // Binds the VAO to be currently used
    void VAO::bind()
    {
        glBindVertexArray(m_identifier);
    }

    // Unbinds the VAO, to avoid faulty actions on VAO
    void VAO::unbind()
    {
        glBindVertexArray(0);
    }
}
