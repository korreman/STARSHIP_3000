#include <Render/DrawScreen.h>
#include <Render/DrawImages.h>
#include <Render/Framebuffer.h>
#include <Render/Texture.h>
#include <System/Settings.h>
#include <Geometry/Sprite.h>

#include <SDL2/SDL.h>
#include <iostream>

namespace Render
{
    FBO &screenFBO = *new FBO;
    Texture &screenTexture = *new Texture;

    int windowWidth {0}, windowHeight {0}, realWidth {0}, realHeight {0};
    int scaledWidth {0}, scaledHeight {0};

    Sprite screenSprite;

    void screenInit()
    {
        SDL_GL_GetDrawableSize(SDL_GL_GetCurrentWindow(),
                &windowWidth, &windowHeight);
        realWidth = System::getSettings().RealWidth;
        realHeight = System::getSettings().RealHeight;
        screenSprite.position = glm::vec2 {realWidth / 2, realHeight / 2};

        // we calculate scaled widths and heights so the framebuffer isn't
        // stretched
        scaledWidth = realWidth;
        scaledHeight = -realHeight; // texture must be flipped

        double realAspectRatio {(double)realWidth / realHeight};
        double windowAspectRatio {(double)windowWidth / windowHeight};
        double ratioConversion = realAspectRatio / windowAspectRatio;

        if (realAspectRatio > windowAspectRatio)
        {
            scaledHeight /= ratioConversion;
        }
        else
        {
            scaledWidth *= ratioConversion;
        }

        // initialize GLObjects
        screenFBO.init();
        screenFBO.bind();
        screenTexture.init(realWidth, realHeight);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, screenTexture, 0);

        if (not screenFBO.isComplete())
        {
            std::cout << "ERROR - Incomplete framebuffer!\n";
        }
    }

    void drawScreen()
    {
        screenFBO.unbind();
        glViewport(0, 0, windowWidth, windowHeight);
        glClear(GL_COLOR_BUFFER_BIT);

        drawImage(screenTexture, 1, scaledWidth, scaledHeight, screenSprite);

        screenFBO.bind();
        glViewport(0, 0, realWidth, realHeight);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    void screenClose()
    {
        delete &screenFBO;
        delete &screenTexture;
    }
}
