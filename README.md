STARSHIP 3000
------
A 2D shoot 'em up game.

## Roadmap:
* Player health and enemy bullets
* HUD
* Explosions
* Menu System
* Sound support
* Editor and tools

## Requirements:
* OpenGL 3.3
* SDL2

## Required to compile:
* CMake
* rsync
* glm
* cereal
