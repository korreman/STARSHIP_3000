#include <Game/Level.h>
#include <Game/Bullet.h>
#include <Game/Player.h>
#include <Game/Background.h>
#include <Game/EnemySpawner.h>
#include <Engine/CollisionHandler.h>
#include <Engine/Container.h>
#include <System/Assets.h>
#include <SDL2/SDL.h>
using glm::vec2;
using Engine::Container;
using Engine::CollisionHandler;

namespace Game
{
    Level::Level(Engine::Game &game) : Scene(game)
    {
        m_playerEntities = std::make_shared<Container>();
        auto enemyEntities {std::make_shared<Container>()};
        auto colHandler {std::make_shared<CollisionHandler>()};

        m_objects.add(colHandler);
        m_objects.add(std::make_shared<Background>());
        m_objects.add(enemyEntities);
        m_objects.add(m_playerEntities);

        colHandler->addPair(*m_playerEntities, *enemyEntities);
        m_player = std::make_shared<Player>();
        m_playerEntities->add(m_player);
        enemyEntities->add(std::make_shared<EnemySpawner>
                ("example_timeline"));
    }

    void Level::update(const double &deltaTime)
    {
        Scene::update(deltaTime);

        if (m_player->shouldShoot())
        {
            m_playerEntities->add(
                std::make_shared<Bullet>(
                    System::getImage("player_bullet"),
                    System::getHitShape("player_bullet"),
                    m_player->getPosition() + vec2( 5.0, 17.0),
                    vec2(0.0, 240.0)));
            m_playerEntities->add(
                std::make_shared<Bullet>(
                    System::getImage("player_bullet"),
                    System::getHitShape("player_bullet"),
                    m_player->getPosition() + vec2(-4.0, 17.0),
                    vec2(0.0, 240.0)));
        }
    }

    void Level::sendKey(SDL_KeyboardEvent key)
    {
        bool pressed {key.state == SDL_PRESSED};
        switch (key.keysym.sym)
        {
            case SDLK_UP:
                m_player->setAcceleration(DIRECTION_UP, pressed); break;
            case SDLK_DOWN:
                m_player->setAcceleration(DIRECTION_DOWN, pressed); break;
            case SDLK_LEFT:
                m_player->setAcceleration(DIRECTION_LEFT, pressed); break;
            case SDLK_RIGHT:
                m_player->setAcceleration(DIRECTION_RIGHT, pressed); break;

            case SDLK_SPACE: m_player->setShooting(pressed); break;
            case SDLK_ESCAPE:
                SDL_Event userEvent;
                userEvent.type = SDL_QUIT;
                SDL_PushEvent(&userEvent);
                break;
        }
    }

    void Level::sendButton(SDL_ControllerButtonEvent event)
    {
        bool pressed {event.state == SDL_PRESSED};
        switch (event.button)
        {
            case SDL_CONTROLLER_BUTTON_A: m_player->setShooting(pressed); break;
        }
    }

    void Level::sendAxis(SDL_ControllerAxisEvent event)
    {
        double scaledValue {(double)event.value / 32767.0};
        if (event.axis == SDL_CONTROLLER_AXIS_LEFTX)
        {
            m_player->setAcceleration(DIRECTION_RIGHT, scaledValue);
        }
        else if (event.axis == SDL_CONTROLLER_AXIS_LEFTY)
        {
            m_player->setAcceleration(DIRECTION_DOWN, scaledValue);
        }
    }
}
