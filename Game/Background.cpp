#include <Game/Background.h>
#include <Engine/Drawable.h>
#include <Geometry/Geometry.h>
#include <System/Settings.h>
#include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>
using Engine::DrawList;
using Engine::DrawableShapes;
using Geometry::outOfBounds;
using System::getSettings;
using glm::vec2;
using glm::vec4;

namespace Game
{

Background::Background()
{
    m_rGen.seed (13397);

    // Update until background is filled
    spawnStar(STAR_BACK);
    while(!outOfBounds(m_stars.getLines()[0].posA))
    {
        update(1.0 / getSettings().MinFPS);
    }
}

void Background::update(const double &deltaTime)
{
    m_stars.update(deltaTime);

    updateSpawnChances(deltaTime);

    // modulus is fine due to large number range and this not being used for
    // statistics
    if (m_PRNG(m_rGen) % m_frontSpawnChance == 0)
    {
        spawnStar(STAR_FRONT);
    }

    if (m_PRNG(m_rGen) % m_backSpawnChance == 0)
    {
        spawnStar(STAR_BACK);
    }
}

std::shared_ptr<DrawList> Background::getDrawables()
{
    return m_stars.getDrawables();
}

void Background::updateSpawnChances(const double &deltaTime)
{
    m_frontSpawnChance = 1.0 / m_frontSpawnRate / deltaTime;
    m_backSpawnChance  = 1.0 / m_backSpawnRate  / deltaTime;
}

void Background::spawnStar(const STARenum &starType)
{
    int xPosition = m_PRNG(m_rGen) % getSettings().RealWidth;
    int realHeight = getSettings().RealHeight;
    if (starType == STAR_FRONT)
    {
        m_stars.add(vec2(xPosition, realHeight - 1),
                    vec2(xPosition, realHeight - 1 + m_frontHeight),
                    vec2(0.0, -m_frontSpeed),
                    vec4(1.0, 1.0, 1.0, 0.70));
    }
    else if (starType == STAR_BACK)
    {

        m_stars.add(vec2(xPosition, realHeight - 1),
                    vec2(xPosition, realHeight - 1 + m_backHeight),
                    vec2(0.0, -m_backSpeed),
                    vec4(1.0, 1.0, 1.0, 0.55));
    }
}

}
