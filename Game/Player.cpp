#include <Game/Player.h>
#include <System/Settings.h>
#include <System/Assets.h>
#include <iostream>
#include <math.h>
using Engine::Entity;
using System::getSettings;
using glm::vec2;

namespace Game
{
    Player::Player()
        : Entity(System::getImage("bull_ship"),
                 System::getHitShape("bull_ship"),
                 vec2{getSettings().RealWidth / 2.0, 40}),
          m_guns(System::getShootingPattern("player"))
        {
            m_speed = 1200;
            m_friction = 1.0 / 40000;
        }

    void Player::update(const double &deltaTime)
    {
        Entity::update(deltaTime);

        GLfloat deltaTimeGL {(GLfloat)deltaTime};

        m_accel = glm::vec2(m_accelDir[DIRECTION_RIGHT] *  m_speed +
                            m_accelDir[DIRECTION_LEFT]  * -m_speed,
                            m_accelDir[DIRECTION_UP]    *  m_speed +
                            m_accelDir[DIRECTION_DOWN]  * -m_speed);

        m_velocity *= pow(m_friction, deltaTimeGL);
        m_velocity += m_accel * deltaTimeGL;

        // shoot if ready
        m_shootingTimer += deltaTime;
        if (m_isShooting and m_shootingTimer >= m_shootingInterval)
        {
            m_shouldShoot = true;
            m_shootingTimer -= m_shootingInterval;
        }
        else
        {
            m_shouldShoot = false;
        }
    }

    bool Player::shouldShoot()
    {
        return m_shouldShoot;
    }

    void Player::setAcceleration(const DIRECTIONenum &direction,
                                 const double &mode)
    {
        m_accelDir[direction] = mode;
    }

    void Player::setShooting(const bool &mode)
    {
        if (mode and not m_isShooting)
        {
            m_isShooting = true;
            if (m_shootingTimer > m_shootingInterval)
            {
                m_shootingTimer = m_shootingInterval;
            }
        }
        else
        {
            m_isShooting = false;
        }
    }
}
