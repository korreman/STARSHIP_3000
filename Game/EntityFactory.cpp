#include <Game/EntityFactory.h>
#include <System/Assets.h>

namespace Game
{
    std::shared_ptr<Bullet> makeBullet(const std::string &name)
    {
        return std::make_shared<Bullet>(System::getImage("player_bullet"),
                                        System::getHitShape("player_bullet"),
                                        glm::vec2 (0));
    }
}
