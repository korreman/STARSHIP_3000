#include <Game/Enemy.h>
#include <Geometry/Path.h>
using Engine::Image;
using Geometry::HitShape;
using Geometry::Path;

namespace Game
{
    Enemy::Enemy(Image &image,
                 HitShape &hitShapes,
                 Path &path,
                 const std::string &shootPatternName,
                 const glm::vec2 &position,
                 const int &health,
                 const int &points)
        : Entity(image, hitShapes, position, glm::vec2()),
          m_path(path),
          m_startPos(position),
          m_points(points)
    {
        m_health = health;
    }

    void Enemy::update(const double &deltaTime)
    {
        Entity::update(deltaTime);
        m_sprite.position = m_path.getPosition(m_startPos, m_elapsedTime);
    }

    void Enemy::damage(const int &damage)
    {
        Entity::damage(damage);
        blit(glm::vec3(1.0f), 0.05);
    }
}
