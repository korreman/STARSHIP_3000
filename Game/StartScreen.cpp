#include <Game/StartScreen.h>
#include <Engine/Game.h>
#include <SDL2/SDL.h>

namespace Game
{
    StartScreen::StartScreen(Engine::Game &game,
                             Engine::Scene &nextScene)
        : Scene(game) , m_nextScene(nextScene) { }

    void StartScreen::sendKey(SDL_KeyboardEvent key)
    {
        if (key.state == SDL_PRESSED)
        {
            switch(key.keysym.sym)
            {
                case SDLK_SPACE: m_game.switchScene(&m_nextScene); break;
                case SDLK_ESCAPE:
                    SDL_Event userEvent;
                    userEvent.type = SDL_QUIT;
                    SDL_PushEvent(&userEvent);
                    break;
            }
        }
    }

    void StartScreen::sendButton(SDL_ControllerButtonEvent event)
    {
        switch (event.button)
        {
            case SDL_CONTROLLER_BUTTON_START:
                m_game.switchScene(&m_nextScene);
                break;
        }
    }
}
