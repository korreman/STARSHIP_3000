#include <Game/ParticleTest.h>

namespace Game
{
    void ParticleTest::update(const double &deltaTime)
    {
        m_elapsedTime += deltaTime;
        while (m_elapsedTime >= 1.5)
        {
            m_particles.makeBurst(BURST_EXPLOSION, glm::vec2{90, 120}, 0, 2, 80);
            m_elapsedTime -= 1.5;
        }
        m_particles.update(deltaTime);
    }

    std::shared_ptr<Engine::DrawList> ParticleTest::getDrawables()
    {
        return m_particles.getDrawables();
    }
}
