#include <Game/ShootingPattern.h>
#include <Game/EntityFactory.h>

namespace Game
{
    ShootingPattern::ShootingPattern()
    {
        m_children = std::make_shared<Engine::ObjectList>();
    }

    void ShootingPattern::update(const double &deltaTime)
    {
        m_timer += deltaTime;
        while (m_timer > m_period)
        {
            m_timer -= m_period;
            addBullets(m_timer);
        }
    }

    void ShootingPattern::setOffset(glm::vec2 &offset)
    {
        m_offset = offset;
    }

    void ShootingPattern::addBullets(const double &elapsedTime)
    {
        for (auto b : m_bullets)
        {
            auto bullet {makeBullet(b.Name)};
            bullet->set(b.Position + m_offset, b.Velocity);
            m_children->push_back(bullet);
        }
    }

    void ShootingPattern::setShooting(const bool &mode)
    {
        if (mode and not m_shooting)
        {
            m_shooting = true;
            if (m_timer > m_period) { m_timer = m_period; }
        }
        else
        {
            m_shooting = false;
        }
    }
}
