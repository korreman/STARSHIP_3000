#include <Game/EnemySpawner.h>
#include <Game/Enemy.h>
#include <Engine/Container.h>
#include <System/Assets.h>
#include <Geometry/Path.h>
using Engine::Container;
using Engine::ObjectList;

#include <cereal/types/list.hpp>
#include <fstream>
using std::string;

#define SPAWN_PREFIX string {"assets/timelines/"}
#define ENEMY_PREFIX string {"assets/enemies/"}
#define EXTENSION string {".json"}
namespace Game
{
    EnemySpawner::EnemySpawner()
        : m_children(std::make_shared<ObjectList>()) {}

    EnemySpawner::EnemySpawner(const string &file)
        : EnemySpawner()
    {
        std::list<SpawnAction> timeline;
        std::ifstream is(SPAWN_PREFIX + file + EXTENSION);
        if (is.good())
        {
            cereal::JSONInputArchive archive(is);
            archive(*this);
        }
    }

    std::shared_ptr<Enemy> EnemySpawner::makeEnemy(const string &enemyName,
            const string &pathName, const glm::vec2 &position)
    {
        EnemyInfo info;
        std::ifstream is(ENEMY_PREFIX + enemyName + EXTENSION);
        if (is.good())
        {
            cereal::JSONInputArchive archive(is);
            archive(info);
        }
        return std::make_shared<Enemy>(
            System::getImage(info.TextureName),
            System::getHitShape(info.TextureName),
            System::getPath(pathName),
            info.ShootingPatternName, position,
            info.Health, info.Points);
    }

    void EnemySpawner::update(const double &deltaTime)
    {
        m_elapsedTime += deltaTime;

        while (m_enemies.begin() != m_enemies.end() &&
               m_elapsedTime >= m_enemies.begin()->SpawnTime)
        {
            auto e = *m_enemies.begin();
            e.enemy->update(m_elapsedTime - m_enemies.begin()->SpawnTime);

            m_children->push_front(e.enemy);
            m_enemies.erase(m_enemies.begin());
        }
    }

    std::shared_ptr<ObjectList> EnemySpawner::getChildren()
    {
        return m_children;
    }
}
