#include <Game/Bullet.h>
#include <Engine/Container.h>
#include <Engine/AnimationEntity.h>
#include <Engine/Image.h>
#include <Geometry/Geometry.h>
#include <Geometry/HitShape.h>
using Engine::Image;
using Geometry::HitShape;
using Geometry::outOfBounds;

namespace Game
{
    Bullet::Bullet(Image &image,
                   Geometry::HitShape &hitShapes,
                   const glm::vec2 &position,
                   const glm::vec2 &velocity,
                   const int &damage)
        : Entity(image, hitShapes, position, velocity), m_damage(damage) {}

    void Bullet::set(const glm::vec2 &position, const glm::vec2 &velocity)
    {
        m_position = position;
        m_velocity = velocity;
    }

    bool Bullet::isAlive()
    {
        return Entity::isAlive() and not
            outOfBounds(m_sprite.position, m_image.Width, m_image.Height);
    }

    void Bullet::collide(Entity &e)
    {
        m_health = 0;
        e.damage(m_damage);
    }
}
