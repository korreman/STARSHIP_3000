TODO
----------------
General:
Fix odd texture dimension rendering issue

1. Add player stats
2. Add sound and music
3. Add decent explosions and some particle effects
4. Create an actual level
5. Improve player movement
6. Create a start screen
7. Pause function.



Tools
----------------
It would be nice to have some tools for testing, creation of certain data,
showcasing, and playing with features. The question is how to implement such
tools.

Idea: Separate executables using the libraries along with some of their own
classes and whatnot
