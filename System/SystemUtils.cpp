#include <System/SystemUtils.h>
#include <cerrno>
#include <fstream>
#include <sstream>
using std::string;

namespace System
{
    string loadTextFile(const char *filename)
    {
        std::ifstream in(filename);
        if (in.good())
        {
            std::stringstream ss;
            ss << in.rdbuf();
            return ss.str();
        }
        throw(errno);
    }
}
