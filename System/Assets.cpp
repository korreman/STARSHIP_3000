#include <System/Assets.h>
#include <System/AssetHandler.h>

#include <Geometry/Path.h>
#include <Geometry/HitShape.h>
#include <Engine/Image.h>
#include <Game/ShootingPattern.h>
using Game::ShootingPattern;

namespace System
{
    AssetHandler<Geometry::Path> paths("assets/paths/", ".json");
    AssetHandler<Geometry::HitShape> hitshapes("assets/hitshapes/", ".json");
    AssetHandler<Engine::Image> images("assets/imageinfo/", ".json");
    AssetHandler<ShootingPattern> patterns("assets/shootingpatterns/", ".json");

    void loadAssets()
    {
        paths.load();
        hitshapes.load();
        images.load();
        patterns.load();
    }

    Geometry::Path& getPath(const std::string &name)
    {
        return paths.get(name);
    }

    Geometry::HitShape& getHitShape(const std::string &name)
    {
        return hitshapes.get(name);
    }

    Engine::Image& getImage(const std::string &name)
    {
        return images.get(name);
    }

    ShootingPattern& getShootingPattern(const std::string &name)
    {
        return patterns.get(name);
    }
}
