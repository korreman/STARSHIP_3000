#include <System/Settings.h>
#include <cereal/archives/xml.hpp>
#include <fstream>

namespace System
{
    Settings settings;

    Settings& getSettings()
    {
        return settings;
    }

    Settings::Settings()
    {
        settings.load();
        settings.save();
    }

    void Settings::save()
    {
        std::ofstream os("settings.xml");
        if (os.good())
        {
            cereal::XMLOutputArchive archive(os);
            archive(CEREAL_NVP(Name),
                    CEREAL_NVP(WindowWidth),
                    CEREAL_NVP(WindowHeight),
                    CEREAL_NVP(RealWidth),
                    CEREAL_NVP(RealHeight),
                    CEREAL_NVP(Fullscreen),
                    CEREAL_NVP(MinFPS),
                    CEREAL_NVP(MaxFPS),
                    CEREAL_NVP(TimeScale));
        }
    }

    void Settings::load()
    {
        std::ifstream is("settings.xml");
        if (is.good())
        {
            cereal::XMLInputArchive archive(is);
            archive(CEREAL_NVP(Name),
                    CEREAL_NVP(WindowWidth),
                    CEREAL_NVP(WindowHeight),
                    CEREAL_NVP(RealWidth),
                    CEREAL_NVP(RealHeight),
                    CEREAL_NVP(Fullscreen),
                    CEREAL_NVP(MinFPS),
                    CEREAL_NVP(MaxFPS),
                    CEREAL_NVP(TimeScale));
        }
    }

}
