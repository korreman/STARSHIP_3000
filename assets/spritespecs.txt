Sprite specs

Things owned by the players and enemies need to be very easy to distinguish from
each other. The bullets need to be cold colors for the players and warm for the
enemies in generel. The enemy ships just need to be very distinguishable from
player itself.

Bullets:

"Fiery bullets" pointing up and pointing diagonally
Plasma orbs - 5x5/6x6, 8x8, 12x12

Ships:

Bugs - Barely ever shoot, smaller than player, weak but plentiful
Soldiers - Same size as player, regular shooting pattern
BombBots - Move towards player, explode, about the same size as player, must
be flashy and combustible
Jizzers - A bit larger than player, shoots bullets everywhere
Tanks - Big ships with lots of hp that shoot bombs and bullets

I can honestly think of a million different types of ships, but those are some
initial ideas

No idea how explosions are gonna work yet, some sort of combination of particles
and animation?
