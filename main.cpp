#include <Engine/Game.h>
#include <Game/Level.h>
#include <Game/StartScreen.h>
#include <memory>

int main()
{
    typedef std::unique_ptr<Engine::Scene> ScenePtr;

    Engine::Game game;
    ScenePtr level(new Game::Level(game));
    ScenePtr startScreen(new Game::StartScreen(game, *level));

    game.run(startScreen.get());

    return 0;
}
